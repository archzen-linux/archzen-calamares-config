
#!/bin/bash

# This script launches `calamares`, using `pkexec` (all the environment variables are passed with `env` option).

pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY QT_QPA_PLATFORMTHEME=$QT_QPA_PLATFORMTHEME XCURSOR_THEME=$XCURSOR_THEME calamares -d
