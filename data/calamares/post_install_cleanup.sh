
#!/bin/bash

# This script performs some post installation procedure.

# Removing stuff, that was only used by the ISO.
rm /etc/systemd/system/etc-pacman.d-gnupg.mount
rm /root/.automated_script.sh
rm /root/.zlogin
rm /etc/polkit-1/rules.d/49-nopasswd_global.rules
rm -r /etc/systemd/system/getty@tty1.service.d
rm /usr/local/bin/Installation_guide

# Remove unwanted packages
pacman-key --init
pacman-key --populate archlinux
pacman -Syu --noconfirm --disable-download-timeout

# Remove GPU Drivers



# Remove Other packages that are only required in the live environment.
pacman -Rns sway gparted cage calamares archzen-calamares-config archinstall arch-install-scripts --noconfirm
